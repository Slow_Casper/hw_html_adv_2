import browserSync from "browser-sync";
const bsServer = browserSync.create();
import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;
import autoprefixer from "gulp-autoprefixer";
import gulpClean from "gulp-clean";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import imageMin from "gulp-imagemin";
import jsMinify from "gulp-js-minify";
import dartSass from "sass";
import gulpSass from "gulp-sass";
const sass = gulpSass(dartSass);
import uglify from "gulp-uglify";
import { deleteSync } from "del";




async function clearFiles () {
    return deleteSync("./dist/**/");
}

function styles() {
    return src("./src/styles/styles.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], { cascade: true, }))
        .pipe(cleanCSS({ compatibility: "ie8" }))
        .pipe(concat("styles.min.css"))
        .pipe(dest("./dist/css"))
        .pipe(bsServer.reload({ stream: true }));
}

function scripts() {
    return src("./src/js/**/*.js")
    .pipe(concat('scripts.min.js'))
    .pipe(jsMinify())
    .pipe(dest("./dist/js"))
    .pipe(bsServer.reload({ stream: true }))
}

function images() {
    return src("./src/img/**/*.{jpg,jpeg,png,svg,webp}")
    .pipe(dest("./dist/img"))
    .pipe(bsServer.reload({ stream: true }))
}

function serve() {
    bsServer.init({
        server: {
            baseDir: "./",
            browser: "chrome",
        },
    });
}

function watcher() {
    watch("./src/styles/**/*.scss", styles);

    watch("*.html").on("change", bsServer.reload);
    watch("./src/js/*.js", scripts);
    watch("./src/img/**/*.{jpg,jpeg,png,svg,webp}").on("change", series(images, bsServer.reload));
}



export const build = series(clearFiles, styles, scripts, images);
export const dev = series(build, parallel(serve, watcher));


