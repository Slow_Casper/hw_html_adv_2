const btn = document.querySelector(".drop");
const menu = document.querySelector(".main-header__nav");


function open (event) {
    btn.classList.toggle("closed");
    menu.classList.toggle("nav--closed")
}

btn.addEventListener("click", open);